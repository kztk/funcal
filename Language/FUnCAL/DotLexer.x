{
{-# OPTIONS_GHC -fno-warn-tabs #-}
{-# LANGUAGE BangPatterns #-}
module Language.FUnCAL.DotLexer where

import Data.Char
}

%wrapper "monad" 

$alpha   = [_a-zA-Z\200-\377] 
@ident   = $alpha ($alpha | [0-9])+
@integer = [0-9]+
@float   = '-'? ('.' [0-9]+ | [0-9]+ '.' [0-9]* )
@string  = \" ( ($printable # [\"]) | "\" [\"] )* \" -- "

@reserved = [nN][oO][dD][eE] | [eE][dD][gG][eE] |
            ([dD][iI]|[sS][uU][bB])?[gG][rR][aA][pP][hH] |
            [sS][tT][rR][iI][cC][tT]

tokens :-
  <0>          "/*" { begin comment }
  <comment>    "*/" { begin 0 } 
  <comment>    [.\n];

  <0> $white+;
  <0> "//".*;
  <0> "->"      { tok $ const Arrow  }
  <0> "--"      { tok $ const Dash  }
  <0> ";"       { tok $ const SemiColon  }
  <0> "{"       { tok $ const LBrace  }
  <0> "}"       { tok $ const RBrace  }
  <0> "["       { tok $ const LBracket  }
  <0> "]"       { tok $ const RBracket  }
  <0> ":"       { tok $ const Colon  }
  <0> ","       { tok $ const Comma  }
  <0> "="       { tok $ const Equal  }
  <0> @reserved { tok $ Key . map toLower }
  <0> @integer  { tok $ Int . read }
  <0> @float    { tok $ Float . read }
  <0> @ident    { tok $ String }
  <0> @string   { tok $ String . unescapeQuote }

{
data Token = Token { offset :: !Int, line :: !Int, column :: !Int, symbol :: !Symbol } | EOF

data Symbol = SemiColon | LBrace | RBrace | LBracket | RBracket | Colon | Equal | Comma
            | Arrow | Dash 
            | Key String | Int Integer | Float Double | String String

instance Show Token where
  show (Token _ _ _ s) = show s
  show EOF             = "<EOF>"
  
instance Show Symbol where
  show SemiColon  = ";"
  show LBrace     = "{"
  show RBrace     = "}"
  show LBracket   = "["
  show RBracket   = "]"
  show Colon      = ":"
  show Comma      = "," 
  show Equal      = "="
  show Arrow      = "->"
  show Dash       = "--"
  show (Key k)    = k
  show (Int i)    = show i
  show (Float f)  = show f
  show (String s) = show s 

unescapeQuote (_:s) = go s
  where
    go [s]            = []
    go ('\\':'"': ss) = '"' : go ss
    go (c:ss)         = c : go ss 
    
getAlexPosn :: Alex AlexPosn
getAlexPosn = Alex $ \s -> Right (s, alex_pos s)

{-# INLINE tok #-}
tok :: (String -> Symbol) -> AlexAction Token
tok f (AlexPn !off !l !c, _, _, str) len = 
  return $ Token off l c (f $ take len str)


alexEOF = return EOF    
}
 
