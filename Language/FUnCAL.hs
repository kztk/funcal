{-# LANGUAGE BangPatterns, NoMonomorphismRestriction #-}
{-# LANGUAGE RankNTypes #-}
module Language.FUnCAL 
    (
      module Language.FUnCAL.Graph
     -- * Types and Classes for Memoization
    , MemoIO, runMemoIO
    , MemoST, runMemoST
    , PMemoT, runPMemoT, runMemo
    , Memoizing
    , run 
      -- * Graph (expression) type
    , G
      -- * Printing Functions 
    , ClosedG
    , close
    , open 
      -- * Graph Constructions
      -- ** Empty Graph 
    , end
      -- ** Edge Extension
    , consG
    , (<:>)
      -- ** Union 
    , unionG
    , unionsG
      -- ** Cyclic Graph Construction
    , gfixG
    , fixG
      -- ** Structural Recursions.
    , gparaG
    , paraG

    , gforG
    , forG
    , gfoldG
    , foldG
      
      -- ** Emptiness Test
    , isEmptyG
      -- ** Combinators to Utilize RecursiveDo (EXPERIMENTAL)
    , graph
    , connect
    , srec
      -- * Evaluation 
    , toGraph
      -- * For Printing Internal Representation
    , asTerm
    , Repl
      -- * Reading Graphs
    , fromGraph
    )  where 

import Control.Monad.Fix

import Language.FUnCAL.Internal
import Language.FUnCAL.Internal.Memo 
import Language.FUnCAL.Graph 
import Language.FUnCAL.Shaped

run :: Ord a =>
       (forall m. (Memoizing m, MonadFix m) => m (G m a)) ->
       Graph a
run m = runMemo (toGraph =<< m)        

-- | Pattern-match like operation
gforG :: (Memoizing m, Shaped t) =>
         Shape t -> (a -> G m a -> m (t (G m a)))
         -> G m a -> m (t (G m a))
gforG !n f = gparaG n (\a x _ -> f a x)
{-# INLINABLE gforG #-}

-- | A specialized version of `gforG` 
forG :: Memoizing m => (a -> G m a -> m (G m a))
                         -> G m a -> m (G m a)
forG f = paraG (\a x _ -> f a x)
{-# INLINABLE forG #-}

-- | Fold-like traversal 
gfoldG :: (Memoizing m, Shaped t) =>
          Shape t ->
          (a -> t (G m a) -> m (t (G m a)))
          -> G m a -> m (t (G m a))
gfoldG !n f = gparaG n (\a _ r -> f a r)
{-# SPECIALIZE
   gfoldG :: Int -> (a -> [G IO a] -> IO [G IO a])
             -> G IO a -> IO [G IO a] #-}
{-# SPECIALIZE
   gfoldG :: Int -> (a -> [G (MemoST s) a] -> MemoST s [G (MemoST s) a])
             -> G (MemoST s) a -> MemoST s [G (MemoST s) a] #-}
{-# INLINABLE gfoldG #-}

-- | A specialized version of `gfoldG`
foldG :: Memoizing m => (a -> G m a -> m (G m a))
                         -> G m a -> m (G m a)
foldG f = paraG (\a _ r -> f a r) 
{-# SPECIALIZE
   foldG :: (a -> G IO a -> IO (G IO a)) -> G IO a -> IO (G IO a)
  #-}
{-# SPECIALIZE
   foldG :: (a -> G (MemoST s) a -> MemoST s (G (MemoST s) a)) -> G (MemoST s) a -> MemoST s (G (MemoST s) a)
  #-}
{-# INLINABLE foldG #-}

