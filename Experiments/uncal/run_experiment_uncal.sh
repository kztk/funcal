#!/bin/sh

UNCALCMD="/Users/kztk/ground_tram/bin/uncalcmd -t"
OPTBASE="-oa -rw"
OPTBIG="-lu -le"
OPTSMALL=""

if [ $# -lt 1 ]; then 
    echo "Needs an argument."
    exit 1
fi 

RECMODE=0
for i in $*; do 
    if [ "$i" = "-ht" ]; then 
        echo "Set -ht"
        OPTBASE="$OPTBASE -ht"        
    elif [ "$i" = "-rec" ]; then 
        echo "Set -rec (instead of bulk semantics)"    
        OPTBASE="$OPTBASE -rec"
        RECMODE=1
    fi 
done

if [ $RECMODE -eq 0 ]; then 
    OPTSMALL="$OPTSMALL -cb"
fi

UNCALOPT="$OPTBASE $OPTSMALL"
UNCALOPTB="$OPTBASE $OPTBIG"


case $1 in
    pim2psm) 
        echo "PIM2PSM"
        $UNCALCMD $UNCALOPT -db ../db/PIM_db1.uncal -q query/PIM2PSM1.uncal        
        ;;
    class2rdb)
        echo "Class2RDB"
        $UNCALCMD $UNCALOPT -dbd ../db/class6.dot -q query/class2rdb6.uncal
        ;; 
    c2o)
        echo "C2O_Sel"
        $UNCALCMD $UNCALOPT -dbd ../db/customers_.dot -q query/c2o_select_demo.uncal
        ;;
    s30k)
        echo "S30k"
        $UNCALCMD $UNCALOPTB -dbd ../db/aseq30000.dot -q query/a2d_xc_n.uncal 
        ;;
    m200)
        echo "m200"
        $UNCALCMD $UNCALOPTB -dbd ../db/lat200.dot -q query/a2d_xc_n.uncal
        ;;
    c200)
        echo "C200"
        $UNCALCMD $UNCALOPTB -dbd ../db/p200.dot -q query/a2d_xc_n.uncal 
        ;;
    *)
        echo "Direct mode" 
        $UNCALCMD $*
esac
