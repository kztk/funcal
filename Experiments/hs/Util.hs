module Util where

import System.CPUTime
import System.IO

import Control.DeepSeq

speedTest :: NFData a => IO a -> IO Double
speedTest m =
  do startTime <- getCPUTime
     f <- m 
     print $ rnf f
     endTime <- getCPUTime
     let elapsed = fromIntegral (endTime - startTime) / 10^9
     putStrLn $ "# Elapsed: " ++ show elapsed ++ " ms"
     return elapsed

speedTests :: NFData a => (b -> IO a) -> [b] -> IO Double
speedTests f bs =
    do startTime <- getCPUTime
       fs <- mapM f bs 
       print $ rnf fs
       endTime <- getCPUTime
       let elapsed = fromIntegral (endTime - startTime) / 10^9
       putStrLn $ "# Total:   " ++ show elapsed ++ " ms"
       putStrLn $ "# Avarage: " ++ show (elapsed / fromIntegral (length bs)) ++ " ms"
       return elapsed    
