module Main where

import qualified Language.FUnCAL as F
import qualified Language.FUnCAL.Graph as F

import qualified Data.Map as M
import qualified Data.Vector as V
import Data.Vector ((!), Vector) 

import qualified System.Console.GetOpt as G
import System.Environment (getArgs)

data Conf = Conf { nCustomer :: Int
                 , nOrder    :: Int }
                   
defaultConf = Conf 10 5

options :: [G.OptDescr (Conf -> Conf)]
options = [ G.Option "cn" ["customers"]
            (G.ReqArg (\s conf -> conf { nCustomer = read s }) "NUM")
            "The number of customers"
          , G.Option "om" ["orders"]
            (G.ReqArg (\s conf -> conf { nOrder = read s }) "NUM")
            "The number of orders par customer" ]

parseOpts :: [String] -> IO Conf
parseOpts args =
  case G.getOpt G.Permute options args of
    (fs, nops, []) ->
      return $ foldl (flip ($)) defaultConf fs
    (fs, nops, es) ->
      fail (concat es ++ G.usageInfo "" options)
          
makeOrder :: F.Memoizable m => Conf -> 
             F.G m F.UnCALLabel -> Int -> Int ->
             m (F.G m F.UnCALLabel)
makeOrder conf x cn on = 
  mconcat 
  [ F.LConst "date"     F.<:> date cn on,
    F.LConst "no"       F.<:> int (cn*nCustomer conf*10 + on), 
    F.LConst "order_of" F.<:> return x ]  
  where
    date i j = str $ show ([1..28] !! (j `mod` 28)) ++ "/" ++
                     show ([1..12] !! (i `mod` 12)) ++ "/2016"

makeCustomer :: F.Memoizable m => Conf -> 
                (F.G m F.UnCALLabel) -> Vector (F.G m F.UnCALLabel) -> Int ->
                m (F.G m F.UnCALLabel)
makeCustomer conf add1 cs i = do
  add2 <- mconcat 
          [ F.LConst "type" F.<:> lab "contractual",
            F.LConst "code" F.<:> str "127.0.0.1",
            F.LConst "info" F.<:> str "there is no place like home" ]            
  a <- mconcat [
    F.LConst "name"  F.<:> str (if i == nCustomer conf - 1 then "Tanaka" else "Customer Name" ++ show i),
    F.LConst "email" F.<:> str ("customer"++show i++"@gmail.com"),
    F.LConst "email" F.<:> str ("c"++show i++"@sf2.ecei"),
    F.LConst "add"   F.<:> return add1,
    F.LConst "add"   F.<:> return add2 ]                 
  b <- F.unionsG =<<
       mapM (\j -> F.LConst "order" F.<:> makeOrder conf (cs ! i) i j)
       [0..nOrder conf - 1]
  F.unionG a b 

str :: F.Memoizable m => String -> m (F.G m F.UnCALLabel)
str s = F.LString s F.<:> F.end

int :: F.Memoizable m => Int -> m (F.G m F.UnCALLabel)
int i = F.LInt i F.<:> F.end

lab s = F.LConst s F.<:> F.end 
  

customers :: F.Memoizable m => Conf -> m (F.G m F.UnCALLabel)
customers conf = do 
  add1 <- F.unionsG =<< sequence 
          [ F.LConst "type" F.<:> lab "shipping",
            F.LConst "code" F.<:> str "999-100",
            F.LConst "info" F.<:> str "Aobayama"]
  xs <- F.gfixG (nCustomer conf)
        (\cs -> V.generateM (nCustomer conf) (makeCustomer conf add1 cs))
  F.unionsG =<< mapM (F.consG (F.LConst "customer")) (V.toList xs)
                          
       
main = do
  conf <- parseOpts =<< getArgs
  g <- F.toGraph =<< customers conf 
  putStrLn (F.dotString $ fmap show g)
