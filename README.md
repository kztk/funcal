FUnCAL
======

What is this?
-------------

This is an embedded implmentation of FUnCAL, proposed in the paper:
_Kazutaka Matsuda and Kazuyuki Asada: "A Functional Reformulation of UnCAL Graph Transformations---Or, Graph Transformation as Graph Reduction", PEPM 2017_.  

This package provides a set of functions for
graph transformations that respect bisimilarity of graphs.
Sharings and cycles are transparent in such bisimilarity-respecting
graphs; i.e., graphs are equivalent to (regular) possibly-infinite trees
under graph bisimilarity. The concept of FUnCAL is to leverage this fact:
defining graphs as infinite trees and transform them by infinite trees.

Here is an example of graph and graph transformation definitions in our system.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ .haskell
friends :: (MonadFix m, Memoizing m) => m (G m String, G m String, G m String)
friends = do
  rec alice <- graph $ ("name" <:> "alice" <:> end)
                       <> ("knows" <:> carol)
      bob   <- graph $ ("name" <:> "bob" <:> end)
                       <> ("knows" <:> alice)
      carol <- graph $ ("name" <:> "carol" <:> end)
                       <> ("knows" <:> (alice <> bob))
  return (alice, bob, carol)

getAquaintances :: (MonadFix m, Memizing m) => G m String -> m (G m String)
getAquaintances = do
  rec f <- srec $ \a x ->
            if      a == "name"  then "nm" <:> return x
            else if a == "knows" then f x
            else                      end
  return f

alice'sAquaintances = do
   (a, _, _) <- friends
   getAquaintances a             

{-
>>> run alice'sAquaintances
R: 1
E: 0,
   1 --[ "nm" ]-> 2,
     --[ "nm" ]-> 4,
     --[ "nm" ]-> 6,
   2 --[ "alice" ]-> 0,
   4 --[ "carol" ]-> 0,
   6 --[ "bob" ]-> 0
-}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


How to Use?
-----------

Just import "Language.FUnCAL".

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Prelude> :l Language.FUnCAL
...
*Language.FUnCAL>
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


### Construction of Graph Data

A singleton graph is made by `end`.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*Language.FUnCAL> :t end
end :: Memoizing m => m (G m a)
*Language.FUnCAL> run $ end
R: 0
E: 0, 1
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


Here, `m (G m a)` is a type for FUnCAL expressions, `run :: Ord a =>
(forall m. (Memoizing m, MonadFix m) => G m a) -> Graph a` evaluates
FUnCAL expressions to graphs.

The library handles rooted graphs. We can add an edge to the root of a
graph by using `(<:>)`.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*Language.FUnCAL> :t (<:>)
(<:>) :: Memoizing m => a -> m (G m a) -> m (G m a)
*Language.FUnCAL> run $ "A" <:> end
R: 1
E: 0, 1 --[ "A" ]-> 0
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


We can take "union" of the set of edges of the root nodes of graphs by
`<>` (defined in `Data.Monoid`).

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*Language.FUnCAL> import Data.Monoid
*Language.FUnCAL> :t (<>)
(<>) :: Monoid m => m -> m -> m
*Language.FUncAL> run $ ("A" <:> end) <> ("B" <:> end) <> ("C" <:> end)
R: 1
E: 0,
   1 --[ "A" ]-> 0,
     --[ "B" ]-> 0,
     --[ "C" ]-> 0
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


A cyclic graph is used by `fixG` or `gfixG`.


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*Language.FUnCAL> :t fixG
fixG :: Memoizing m => (G m a -> m (G m a)) -> m (G m a)
*Language.FUnCAL> :t gfixG
gfixG
  :: (Shaped t, Memoizing m) =>
       Shape t -> (t (G m a) -> m (t (G m a))) -> m (t (G m a))    gfixG
*Language.FUnCAL> run $ fixG (\x -> "A" <:> "B" <:> return x)
R: 1
E: 0, 1 --[ "A" ]-> 2, 2 --[ "B" ]-> 1
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


The function `gfixG` allow mutual recursive definitions as follows.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*Language.FUnCAL> run $ head <$> gfixG 2 (\[x,y] -> sequence ["A" <:> return y, "B" <:> return x])
R: 1
E: 0, 1 --[ "A" ]-> 2, 2 --[ "B" ]-> 1
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


The function `gfixG` can also be used to introduce sharings. But,
there is no way to know whether a node is shared or not in FUnCAL, as
it repects graph bisimulation.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*Language.FUnCAL> run $ head <$> gfixG 2 (\[x,y] -> sequence [mconcat ["A" <:> return y, "B" <:> return y], "C" <:> end])
R: 1
E: 0,
   1 --[ "A" ]-> 2,
     --[ "B" ]-> 2,
   2 --[ "C" ]-> 0
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The library also provides the functions `graph` and `connect`,
which are useful with the "RecursiveDo" extension.

Instead of writing

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ .haskell
graph1 = head <$> gfixG 2 (\[x,y] ->
  sequence [ "A" <:> return y,
             "B" <:> return x ]
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

one can write  

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ .haskell
graph1 = do
  rec x <- graph $ "A" <:> return y
      y <- graph $ "B" <:> return x
  return x
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

or

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ .haskell
graph1 = do
  rec x <- "A" <:> connect y
      y <- "B" <:> connect x
  return x
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This notation is sometimes convinient, but users are required to put
`graph` or `connect` for every recursive definitions. Otherwise,
evaluation may not terminate.

### Transformation of Graphs

The transformations are written by using structural recursions `paraG`
and `gparaG`.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*Language.FUnCAL> :t paraG
paraG
  :: Memoizing m =>
     (a -> G m a -> G m a -> m (G m a)) -> G m a -> m (G m a)
*Language.FUnCAL> :t gparaG
gparaG
  :: (Shaped t, Memoizing m) =>
     Shape t
     -> (a -> G m a -> t (G m a) -> m (t (G m a)))
     -> G m a
     -> m (t (G m a))
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Here is an example of a transformation that "doubles" the edges.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*Language.FUnCAL> :t paraG (\l _ r -> l <:> l <:> return r)
paraG (\l _ r -> l <:> l <:> return r)
  :: Memoizing m => G m a -> m (G m a)
*Language.FUnCAL> run $ fixG (\x -> "A" <:> return x)
R: 1
E: 0, 1 --[ "A" ]-> 1
*Language.FUnCAL> run $ paraG (\l _ r -> l <:> l <:> return r) =<< fixG (\x -> "A" <:> return x)
R: 1
E: 0, 1 --[ "A" ]-> 2, 2 --[ "A" ]-> 1
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The function `gparaG` is a tupled version `paraG`.

The library also provides `srec` to utilize "RecursiveDo". For non
one-liners, this function would be more useful. Instead of writing

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ .haskell
hasAA t = do
  let f = gfoldG 2 (\a [x,y] -> sequence $
                         if a == "A" then [ return y, "dummy" <:> end ]
                         else             [ return x,  end ] )
  not <$> (f t >>= isEmptyG)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


we can write as follows.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ .haskell
hasAA t = do
    rec f <- srec $ \a x ->
               if a == "A" then g x
               else             f x
        g <- srec $ \a x ->
               if a == "A" then "dummy" <:> end
               else             end
    not <$> (f t >>= isEmptyG)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

But, with this `srec` interface, be careful that the argument of
recursive calls must be `x` of `srec $ \a x -> ...`.

Using structure recursions (`gparaG`, `paraG` or `srec`) could be a
source of nontermination. One can avoid nontermination by enforcing
stratified use of graphs, i.e., avoiding the situation that an output of
a recursive function affects its input. In our PEPM 17 paper, we proposed
a type system to get rid of situation, but this type system is not
implemented in this Haskell implementation either statically or
dynamically.

How to Install
--------------

Using `stack` or `cabal` would be the easiest way. Please follow the
standard installation process of these tools.


Known Issues
-------------

FUnCAL assumes lazy evaluation based on Launchbury's natural semantics.
However, GHC, at least versions 7.8.X, 7.10.X and 8.0.X, the semantics is
a bit different from Lauchbury's in the treatment of black holes (e.g.,
`let x = x in x` does not always raise an exception in GHC, but FUnCAL
assume that). So, we use a monad to express our laziness explicitly and
use speciall combinators for recursive definitions, although we provide
some combinators (`graph`, `connect`, and `srec`) that enable us to use
Haskell's recursive defintion.

Also, FUnCAL's type system is not implemented. In our preliminary study,
naive implementation by type-level natural numbers turned out to be
impractical as it requires manual coercions.

In some future, we will address these issues by deep embedding with
loop detection techniques via physical equaly testing, and by
meta-programming techniques such as Template Haskell.

Contact Information
-------------------

Kazutaka Matsuda (kztk "at" is "dot" ecei "dot" tohoku "dot" ac "dot" jp)
