{-# LANGUAGE NoMonomorphismRestriction #-}
module DB.Class6 where 

import Language.FUnCAL

import Data.List
import Control.Monad 
import Util

db :: LazyG Memo Label
db = 
       (nthM 21) (gfixG 22 (\ t236 ->
                              return
                                [unionsG [(LConst "name") >: (LConst "String") >: (LString "address") >: end,
                                          (LConst "src") >: (LConst "Class") >: ((nthM 8) t236),
                                          (LConst "dest") >: (LConst "Class") >: ((nthM 9) t236)],
                                 unionsG [(LConst "name") >: (LConst "String") >: (LString "phone") >: end,
                                          (LConst "src") >: (LConst "Class") >: ((nthM 9) t236),
                                          (LConst "dest") >: (LConst "Class") >: ((nthM 10) t236)],
                                 unionsG [(LConst "name") >: (LConst "String") >: (LString "name") >: end,
                                          (LConst "is_primary") >: ((nthM 13) t236),
                                          (LConst "type") >: ((nthM 11) t236)],
                                 unionsG [(LConst "name") >: (LConst "String") >: (LString "addr") >: end,
                                          (LConst "is_primary") >: ((nthM 14) t236),
                                          (LConst "type") >: ((nthM 11) t236)],
                                 unionsG [(LConst "name") >: (LConst "String") >: (LString "number") >: end,
                                          (LConst "is_primary") >: ((nthM 15) t236),
                                          (LConst "type") >: ((nthM 12) t236)],
                                 (LConst "Attribute") >: ((nthM 2) t236),
                                 (LConst "Attribute") >: ((nthM 3) t236),
                                 (LConst "Attribute") >: ((nthM 4) t236),
                                 unionsG [(LConst "name") >: (LConst "String") >: (LString "Person") >: end,
                                          (LConst "is_persistent") >: ((nthM 16) t236),
                                          (LConst "attrs") >: ((nthM 5) t236),
                                          (LConst "src_of") >: (LConst "Association") >: ((nthM 0) t236)],
                                 unionsG [(LConst "name") >: (LConst "String") >: (LString "Address") >: end,
                                          (LConst "is_persistent") >: ((nthM 17) t236),
                                          (LConst "attrs") >: ((nthM 6) t236),
                                          (LConst "src_of") >: (LConst "Association") >: ((nthM 1) t236)],
                                 unionsG [(LConst "name") >: (LConst "String") >: (LString "Phone") >: end,
                                          (LConst "is_persistent") >: ((nthM 18) t236),
                                          (LConst "attrs") >: ((nthM 7) t236)],
                                 (LConst "PrimitiveDataType") >: ((nthM 19) t236),
                                 (LConst "PrimitiveDataType") >: ((nthM 20) t236),
                                 (LConst "Boolean") >: (LBool True) >: end,
                                 (LConst "Boolean") >: (LBool True) >: end,
                                 (LConst "Boolean") >: (LBool True) >: end,
                                 (LConst "Boolean") >: (LBool True) >: end,
                                 (LConst "Boolean") >: (LBool False) >: end,
                                 (LConst "Boolean") >: (LBool True) >: end,
                                 (LConst "name") >: (LConst "String") >: (LString "String") >: end,
                                 (LConst "name") >: (LConst "String") >: (LString "Integer") >: end,
                                 unionsG [(LConst "Association") >: ((nthM 0) t236),
                                          (LConst "Association") >: ((nthM 1) t236)]]))


query :: LazyG Memo Label -> LazyG Memo Label 
query xdb =
    (\ xtables ->
       (paraG (\ xL10 xfv6 t833 ->
                 if xL10 == (LConst "Table") then
                   xL10 >: ((paraG (\ xL12 xfv7 t823 ->
                                      if xL12 == (LConst "fkeys") then
                                        xL12 >: ((paraG (\ xL14 xfv8 t813 ->
                                                           if xL14 == (LConst "Fkey") then
                                                             xL14 >: ((paraG (\ xL16 xref t803 ->
                                                                                if xL16 == (LConst "ref") then
                                                                                  ifM
                                                                                  (isEmptyG ((paraG (\ xL xtable t743 ->
                                                                                                       if xL == (LConst "Table") then
                                                                                                         (paraG (\ xL xfv4 t735 ->
                                                                                                                   if xL == (LConst "String") then
                                                                                                                     (paraG (\ xrname xfv5 t727 ->
                                                                                                                               (paraG (\ xL xfv1 t720 ->
                                                                                                                                         if xL == (LConst "name") then
                                                                                                                                           (paraG (\ xL xfv2 t712 ->
                                                                                                                                                     if xL == (LConst "String") then
                                                                                                                                                       (paraG (\ xtname xfv3 t704 ->
                                                                                                                                                                 if xtname == xrname then
                                                                                                                                                                   (LConst "ref") >: end
                                                                                                                                                                 else
                                                                                                                                                                   end)) xfv2
                                                                                                                                                     else
                                                                                                                                                       end)) xfv1
                                                                                                                                         else
                                                                                                                                           end)) xtable)) xfv4
                                                                                                                   else
                                                                                                                     end)) xref
                                                                                                       else
                                                                                                         end)) xtables))
                                                                                  (xL16 >: xref)
                                                                                  (xL16 >: ((paraG (\ xL xtable t791 ->
                                                                                                      if xL == (LConst "Table") then
                                                                                                        (paraG (\ xL xfv4 t783 ->
                                                                                                                  if xL == (LConst "String") then
                                                                                                                    (paraG (\ xrname xfv5 t775 ->
                                                                                                                              (paraG (\ xL xfv1 t768 ->
                                                                                                                                        if xL == (LConst "name") then
                                                                                                                                          (paraG (\ xL xfv2 t760 ->
                                                                                                                                                    if xL == (LConst "String") then
                                                                                                                                                      (paraG (\ xtname xfv3 t752 ->
                                                                                                                                                                if xtname == xrname then
                                                                                                                                                                  (LConst "Table") >: xtable
                                                                                                                                                                else
                                                                                                                                                                  end)) xfv2
                                                                                                                                                    else
                                                                                                                                                      end)) xfv1
                                                                                                                                        else
                                                                                                                                          end)) xtable)) xfv4
                                                                                                                  else
                                                                                                                    end)) xref
                                                                                                      else
                                                                                                        end)) xtables))
                                                                                else
                                                                                  xL16 >: xref)) xfv8)
                                                           else
                                                             xL14 >: xfv8)) xfv7)
                                      else
                                        xL12 >: xfv7)) xfv6)
                 else
                   xL10 >: xfv6)) xtables) (paraG (\ xL xclass t695 ->
                                                     if xL == (LConst "Class") then
                                                       (paraG (\ xL xfv47 t641 ->
                                                                 if xL == (LConst "is_persistent") then
                                                                   (paraG (\ xL xfv48 t633 ->
                                                                             if xL == (LConst "Boolean") then
                                                                               (paraG (\ xL xanyv49 t625 ->
                                                                                         if xL == (LBool True) then
                                                                                           (\ xdests ->
                                                                                              paraG (\ xL41 xtable t617 ->
                                                                                                       if xL41 == (LConst "Table") then
                                                                                                         ifM
                                                                                                         (isEmptyG ((paraG (\ xL xcols t408 ->
                                                                                                                              if xL == (LConst "cols") then
                                                                                                                                (paraG (\ xL xfv36 t400 ->
                                                                                                                                          if xL == (LConst "Column") then
                                                                                                                                            (paraG (\ xL xfv37 t392 ->
                                                                                                                                                      if xL == (LConst "name") then
                                                                                                                                                        (paraG (\ xL xfv38 t384 ->
                                                                                                                                                                  if xL == (LConst "String") then
                                                                                                                                                                    (paraG (\ xcname xfv39 t376 ->
                                                                                                                                                                              (LConst "Table") >: end)) xfv38
                                                                                                                                                                  else
                                                                                                                                                                    end)) xfv37
                                                                                                                                                      else
                                                                                                                                                        end)) xfv36
                                                                                                                                          else
                                                                                                                                            end)) xcols
                                                                                                                              else
                                                                                                                                end)) xtable))
                                                                                                         (xL41 >: xtable)
                                                                                                         (xL41 >: ((paraG (\ xL xcols t605 ->
                                                                                                                             if xL == (LConst "cols") then
                                                                                                                               (paraG (\ xL xfv36 t597 ->
                                                                                                                                         if xL == (LConst "Column") then
                                                                                                                                           (paraG (\ xL xfv37 t589 ->
                                                                                                                                                     if xL == (LConst "name") then
                                                                                                                                                       (paraG (\ xL xfv38 t581 ->
                                                                                                                                                                 if xL == (LConst "String") then
                                                                                                                                                                   (paraG (\ xcname xfv39 t573 ->
                                                                                                                                                                             unionsG [xtable,
                                                                                                                                                                                      (paraG (\ xL xfv28 t473 ->
                                                                                                                                                                                                if xL == (LConst "attrs") then
                                                                                                                                                                                                  (paraG (\ xL xfv29 t465 ->
                                                                                                                                                                                                            if xL == (LConst "Attribute") then
                                                                                                                                                                                                              (paraG (\ xL xfv33 t457 ->
                                                                                                                                                                                                                        if xL == (LConst "is_primary") then
                                                                                                                                                                                                                          (paraG (\ xL xfv34 t449 ->
                                                                                                                                                                                                                                    if xL == (LConst "Boolean") then
                                                                                                                                                                                                                                      (paraG (\ xL xanyv35 t441 ->
                                                                                                                                                                                                                                                if xL == (LBool True) then
                                                                                                                                                                                                                                                  (paraG (\ xL xfv30 t433 ->
                                                                                                                                                                                                                                                            if xL == (LConst "name") then
                                                                                                                                                                                                                                                              (paraG (\ xL xfv31 t425 ->
                                                                                                                                                                                                                                                                        if xL == (LConst "String") then
                                                                                                                                                                                                                                                                          (paraG (\ xpname xfv32 t417 ->
                                                                                                                                                                                                                                                                                    if xcname == xpname then
                                                                                                                                                                                                                                                                                      (LConst "pkey") >: xcols
                                                                                                                                                                                                                                                                                    else
                                                                                                                                                                                                                                                                                      end)) xfv31
                                                                                                                                                                                                                                                                        else
                                                                                                                                                                                                                                                                          end)) xfv30
                                                                                                                                                                                                                                                            else
                                                                                                                                                                                                                                                              end)) xfv29
                                                                                                                                                                                                                                                else
                                                                                                                                                                                                                                                  end)) xfv34
                                                                                                                                                                                                                                    else
                                                                                                                                                                                                                                      end)) xfv33
                                                                                                                                                                                                                        else
                                                                                                                                                                                                                          end)) xfv29
                                                                                                                                                                                                            else
                                                                                                                                                                                                              end)) xfv28
                                                                                                                                                                                                else
                                                                                                                                                                                                  end)) xclass,
                                                                                                                                                                                      (paraG (\ xL xfv19 t560 ->
                                                                                                                                                                                                if xL == (LConst "Class") then
                                                                                                                                                                                                  (paraG (\ xL xfv25 t552 ->
                                                                                                                                                                                                            if xL == (LConst "is_persistent") then
                                                                                                                                                                                                              (paraG (\ xL xfv26 t544 ->
                                                                                                                                                                                                                        if xL == (LConst "Boolean") then
                                                                                                                                                                                                                          (paraG (\ xL xanyv27 t536 ->
                                                                                                                                                                                                                                    if xL == (LBool True) then
                                                                                                                                                                                                                                      (paraG (\ xL xfv20 t528 ->
                                                                                                                                                                                                                                                if xL == (LConst "attrs") then
                                                                                                                                                                                                                                                  (paraG (\ xL xfv21 t520 ->
                                                                                                                                                                                                                                                            if xL == (LConst "Attribute") then
                                                                                                                                                                                                                                                              (paraG (\ xL xfv22 t512 ->
                                                                                                                                                                                                                                                                        if xL == (LConst "name") then
                                                                                                                                                                                                                                                                          (paraG (\ xL xfv23 t504 ->
                                                                                                                                                                                                                                                                                    if xL == (LConst "String") then
                                                                                                                                                                                                                                                                                      (paraG (\ xaname xfv24 t496 ->
                                                                                                                                                                                                                                                                                                (paraG (\ xL xdcname t489 ->
                                                                                                                                                                                                                                                                                                          if xL == (LConst "name") then
                                                                                                                                                                                                                                                                                                            if xcname == xaname then
                                                                                                                                                                                                                                                                                                              (LConst "fkeys") >: (LConst "Fkey") >: (unionsG [(LConst "cols") >: xcols,
                                                                                                                                                                                                                                                                                                                                                               (LConst "ref") >: xdcname])
                                                                                                                                                                                                                                                                                                            else
                                                                                                                                                                                                                                                                                                              end
                                                                                                                                                                                                                                                                                                          else
                                                                                                                                                                                                                                                                                                            end)) xfv19)) xfv23
                                                                                                                                                                                                                                                                                    else
                                                                                                                                                                                                                                                                                      end)) xfv22
                                                                                                                                                                                                                                                                        else
                                                                                                                                                                                                                                                                          end)) xfv21
                                                                                                                                                                                                                                                            else
                                                                                                                                                                                                                                                              end)) xfv20
                                                                                                                                                                                                                                                else
                                                                                                                                                                                                                                                  end)) xfv19
                                                                                                                                                                                                                                    else
                                                                                                                                                                                                                                      end)) xfv26
                                                                                                                                                                                                                        else
                                                                                                                                                                                                                          end)) xfv25
                                                                                                                                                                                                            else
                                                                                                                                                                                                              end)) xfv19
                                                                                                                                                                                                else
                                                                                                                                                                                                  end)) xdests])) xfv38
                                                                                                                                                                 else
                                                                                                                                                                   end)) xfv37
                                                                                                                                                     else
                                                                                                                                                       end)) xfv36
                                                                                                                                         else
                                                                                                                                           end)) xcols
                                                                                                                             else
                                                                                                                               end)) xtable))
                                                                                                       else
                                                                                                         xL41 >: xtable) ((paraG (\ xL xcname t216 ->
                                                                                                                                    if xL == (LConst "name") then
                                                                                                                                      (LConst "Table") >: (unionsG [(LConst "name") >: xcname,
                                                                                                                                                                    paraG (\ xL xfv44 t203 ->
                                                                                                                                                                             if xL == (LConst "Class") then
                                                                                                                                                                               (paraG (\ xL xfv45 t195 ->
                                                                                                                                                                                         if xL == (LConst "attrs") then
                                                                                                                                                                                           (paraG (\ xL xfv46 t187 ->
                                                                                                                                                                                                     if xL == (LConst "Attribute") then
                                                                                                                                                                                                       (paraG (\ xL xcname t179 ->
                                                                                                                                                                                                                 if xL == (LConst "name") then
                                                                                                                                                                                                                   (paraG (\ xL xctype t171 ->
                                                                                                                                                                                                                             if xL == (LConst "type") then
                                                                                                                                                                                                                               (LConst "cols") >: (LConst "Column") >: (unionsG [(LConst "name") >: xcname,
                                                                                                                                                                                                                                                                                 (LConst "type") >: xctype])
                                                                                                                                                                                                                             else
                                                                                                                                                                                                                               end)) xfv46
                                                                                                                                                                                                                 else
                                                                                                                                                                                                                   end)) xfv46
                                                                                                                                                                                                     else
                                                                                                                                                                                                       end)) xfv45
                                                                                                                                                                                         else
                                                                                                                                                                                           end)) xfv44
                                                                                                                                                                             else
                                                                                                                                                                               end) (unionsG [(LConst "Class") >: xclass,
                                                                                                                                                                                              xdests])])
                                                                                                                                    else
                                                                                                                                      end)) xclass)) ((nthM 0) ((gparaG 9 (\ xL146 xdest t147 ->
                                                                                                                                                                             if xL146 == (LConst "Association") then
                                                                                                                                                                               return
                                                                                                                                                                                 [end,
                                                                                                                                                                                  end,
                                                                                                                                                                                  end,
                                                                                                                                                                                  end,
                                                                                                                                                                                  end,
                                                                                                                                                                                  (nthM 4) t147,
                                                                                                                                                                                  end,
                                                                                                                                                                                  end,
                                                                                                                                                                                  (nthM 7) t147]
                                                                                                                                                                             else
                                                                                                                                                                               if xL146 == (LConst "Class") then
                                                                                                                                                                                 return
                                                                                                                                                                                   [end,
                                                                                                                                                                                    end,
                                                                                                                                                                                    end,
                                                                                                                                                                                    unionsG [(nthM 1) t147,
                                                                                                                                                                                             (LConst "Class") >: xdest],
                                                                                                                                                                                    end,
                                                                                                                                                                                    end,
                                                                                                                                                                                    unionsG [(nthM 2) t147,
                                                                                                                                                                                             (LConst "Class") >: xdest],
                                                                                                                                                                                    end,
                                                                                                                                                                                    end]
                                                                                                                                                                               else
                                                                                                                                                                                 if xL146 == (LConst "dest") then
                                                                                                                                                                                   return
                                                                                                                                                                                     [end,
                                                                                                                                                                                      end,
                                                                                                                                                                                      end,
                                                                                                                                                                                      end,
                                                                                                                                                                                      (nthM 3) t147,
                                                                                                                                                                                      end,
                                                                                                                                                                                      end,
                                                                                                                                                                                      (nthM 6) t147,
                                                                                                                                                                                      end]
                                                                                                                                                                                 else
                                                                                                                                                                                   if xL146 == (LConst "src_of") then
                                                                                                                                                                                     return
                                                                                                                                                                                       [(nthM 5) t147,
                                                                                                                                                                                        (nthM 8) t147,
                                                                                                                                                                                        (nthM 8) t147,
                                                                                                                                                                                        end,
                                                                                                                                                                                        end,
                                                                                                                                                                                        end,
                                                                                                                                                                                        end,
                                                                                                                                                                                        end,
                                                                                                                                                                                        end]
                                                                                                                                                                                   else
                                                                                                                                                                                     return
                                                                                                                                                                                       [end,
                                                                                                                                                                                        end,
                                                                                                                                                                                        end,
                                                                                                                                                                                        end,
                                                                                                                                                                                        end,
                                                                                                                                                                                        end,
                                                                                                                                                                                        end,
                                                                                                                                                                                        end,
                                                                                                                                                                                        end])) xclass))
                                                                                         else
                                                                                           end)) xfv48
                                                                             else
                                                                               end)) xfv47
                                                                 else
                                                                   end)) xclass
                                                     else
                                                       end) ((nthM 0) ((gparaG 3 (\ xL70 xasc t687 ->
                                                                                    if xL70 == (LConst "Association") then
                                                                                      return
                                                                                        [(nthM 2) t687,
                                                                                         end,
                                                                                         end]
                                                                                    else
                                                                                      if xL70 == (LConst "dest") then
                                                                                        return
                                                                                          [end,
                                                                                           end,
                                                                                           unionsG [(nthM 1) t687,
                                                                                                    xasc]]
                                                                                      else
                                                                                        if xL70 == (LConst "src") then
                                                                                          return
                                                                                            [end,
                                                                                             end,
                                                                                             unionsG [(nthM 1) t687,
                                                                                                      xasc]]
                                                                                        else
                                                                                          return
                                                                                            [end,
                                                                                             end,
                                                                                             end])) xdb)))




query2 :: ([t] -> LazyG Memo Label) -> [t] -> LazyG Memo Label 
query2 xdb = 
    (\ xtables t946 ->
       paraG (\ xL10 t943 ->
                (\ xfv6 t945 ->
                   if xL10 == (LConst "Table") then
                     xL10 >: (paraG (\ xL12 t933 ->
                                       (\ xfv7 t935 ->
                                          if xL12 == (LConst "fkeys") then
                                            xL12 >: (paraG (\ xL14 t923 ->
                                                              (\ xfv8 t925 ->
                                                                 if xL14 == (LConst "Fkey") then
                                                                   xL14 >: (paraG (\ xL16 t913 ->
                                                                                     (\ xref t915 ->
                                                                                        if xL16 == (LConst "ref") then
                                                                                          ifM
                                                                                          (isEmptyG (paraG (\ xL t848 t850 ->
                                                                                                              if xL == (LConst "Table") then
                                                                                                                paraG (\ xL t839 t841 ->
                                                                                                                         if xL == (LConst "String") then
                                                                                                                           (paraG (\ xrname t830 t832 ->
                                                                                                                                     (paraG (\ xL t823 t825 ->
                                                                                                                                               if xL == (LConst "name") then
                                                                                                                                                 (paraG (\ xL t814 t816 ->
                                                                                                                                                           if xL == (LConst "String") then
                                                                                                                                                             (paraG (\ xtname t805 t807 ->
                                                                                                                                                                       if xtname == xrname then
                                                                                                                                                                         (LConst "ref") >: end
                                                                                                                                                                       else
                                                                                                                                                                         end)) t814
                                                                                                                                                           else
                                                                                                                                                             end)) t823
                                                                                                                                               else
                                                                                                                                                 end)) t848)) t839
                                                                                                                         else
                                                                                                                           end) (xref [])
                                                                                                              else
                                                                                                                end) (xtables [])))
                                                                                          (xL16 >: (xref []))
                                                                                          (xL16 >: (paraG (\ xL t901 ->
                                                                                                             (\ xtable t903 ->
                                                                                                                if xL == (LConst "Table") then
                                                                                                                  paraG (\ xL t892 t894 ->
                                                                                                                           if xL == (LConst "String") then
                                                                                                                             (paraG (\ xrname t883 t885 ->
                                                                                                                                       paraG (\ xL t876 t878 ->
                                                                                                                                                if xL == (LConst "name") then
                                                                                                                                                  (paraG (\ xL t867 t869 ->
                                                                                                                                                            if xL == (LConst "String") then
                                                                                                                                                              (paraG (\ xtname t858 t860 ->
                                                                                                                                                                        if xtname == xrname then
                                                                                                                                                                          (LConst "Table") >: (xtable [])
                                                                                                                                                                        else
                                                                                                                                                                          end)) t867
                                                                                                                                                            else
                                                                                                                                                              end)) t876
                                                                                                                                                else
                                                                                                                                                  end) (xtable []))) t892
                                                                                                                           else
                                                                                                                             end) (xref [])
                                                                                                                else
                                                                                                                  end) (\ t902 ->
                                                                                                                          t901)) (xtables [])))
                                                                                        else
                                                                                          xL16 >: (xref [])) (\ t914 ->
                                                                                                                t913)) (xfv8 []))
                                                                 else
                                                                   xL14 >: (xfv8 [])) (\ t924 ->
                                                                                         t923)) (xfv7 []))
                                          else
                                            xL12 >: (xfv7 [])) (\ t934 -> t933)) (xfv6 []))
                   else
                     xL10 >: (xfv6 [])) (\ t944 -> t943)) (xtables t946)) (\ t797 ->
                                                                             paraG (\ xL t794 ->
                                                                                      (\ xclass t796 ->
                                                                                         if xL == (LConst "Class") then
                                                                                           paraG (\ xL t730 t732 ->
                                                                                                    if xL == (LConst "is_persistent") then
                                                                                                      (paraG (\ xL t721 t723 ->
                                                                                                                if xL == (LConst "Boolean") then
                                                                                                                  (paraG (\ xL t712 t714 ->
                                                                                                                            if xL == (LBool True) then
                                                                                                                              ((\ xdests t706 ->
                                                                                                                                  paraG (\ xL41 t703 ->
                                                                                                                                           (\ xtable t705 ->
                                                                                                                                              if xL41 == (LConst "Table") then
                                                                                                                                                ifM
                                                                                                                                                (isEmptyG (paraG (\ xL t472 t474 ->
                                                                                                                                                                    if xL == (LConst "cols") then
                                                                                                                                                                      (paraG (\ xL t463 t465 ->
                                                                                                                                                                                if xL == (LConst "Column") then
                                                                                                                                                                                  (paraG (\ xL t454 t456 ->
                                                                                                                                                                                            if xL == (LConst "name") then
                                                                                                                                                                                              (paraG (\ xL t445 t447 ->
                                                                                                                                                                                                        if xL == (LConst "String") then
                                                                                                                                                                                                          (paraG (\ xcname t436 t438 ->
                                                                                                                                                                                                                    (LConst "Table") >: end)) t445
                                                                                                                                                                                                        else
                                                                                                                                                                                                          end)) t454
                                                                                                                                                                                            else
                                                                                                                                                                                              end)) t463
                                                                                                                                                                                else
                                                                                                                                                                                  end)) t472
                                                                                                                                                                    else
                                                                                                                                                                      end) (xtable [])))
                                                                                                                                                (xL41 >: (xtable []))
                                                                                                                                                (xL41 >: (paraG (\ xL t691 ->
                                                                                                                                                                   (\ xcols t693 ->
                                                                                                                                                                      if xL == (LConst "cols") then
                                                                                                                                                                        paraG (\ xL t682 t684 ->
                                                                                                                                                                                 if xL == (LConst "Column") then
                                                                                                                                                                                   (paraG (\ xL t673 t675 ->
                                                                                                                                                                                             if xL == (LConst "name") then
                                                                                                                                                                                               (paraG (\ xL t664 t666 ->
                                                                                                                                                                                                         if xL == (LConst "String") then
                                                                                                                                                                                                           (paraG (\ xcname t655 t657 ->
                                                                                                                                                                                                                     unionsG [xtable [],
                                                                                                                                                                                                                              paraG (\ xL t545 t547 ->
                                                                                                                                                                                                                                       if xL == (LConst "attrs") then
                                                                                                                                                                                                                                         (paraG (\ xL t536 ->
                                                                                                                                                                                                                                                   (\ xfv29 t538 ->
                                                                                                                                                                                                                                                      if xL == (LConst "Attribute") then
                                                                                                                                                                                                                                                        paraG (\ xL t527 t529 ->
                                                                                                                                                                                                                                                                 if xL == (LConst "is_primary") then
                                                                                                                                                                                                                                                                   (paraG (\ xL t518 t520 ->
                                                                                                                                                                                                                                                                             if xL == (LConst "Boolean") then
                                                                                                                                                                                                                                                                               (paraG (\ xL t509 t511 ->
                                                                                                                                                                                                                                                                                         if xL == (LBool True) then
                                                                                                                                                                                                                                                                                           paraG (\ xL t500 t502 ->
                                                                                                                                                                                                                                                                                                    if xL == (LConst "name") then
                                                                                                                                                                                                                                                                                                      (paraG (\ xL t491 t493 ->
                                                                                                                                                                                                                                                                                                                if xL == (LConst "String") then
                                                                                                                                                                                                                                                                                                                  (paraG (\ xpname t482 t484 ->
                                                                                                                                                                                                                                                                                                                            if xcname == xpname then
                                                                                                                                                                                                                                                                                                                              (LConst "pkey") >: (xcols [])
                                                                                                                                                                                                                                                                                                                            else
                                                                                                                                                                                                                                                                                                                              end)) t491
                                                                                                                                                                                                                                                                                                                else
                                                                                                                                                                                                                                                                                                                  end)) t500
                                                                                                                                                                                                                                                                                                    else
                                                                                                                                                                                                                                                                                                      end) (xfv29 [])
                                                                                                                                                                                                                                                                                         else
                                                                                                                                                                                                                                                                                           end)) t518
                                                                                                                                                                                                                                                                             else
                                                                                                                                                                                                                                                                               end)) t527
                                                                                                                                                                                                                                                                 else
                                                                                                                                                                                                                                                                   end) (xfv29 [])
                                                                                                                                                                                                                                                      else
                                                                                                                                                                                                                                                        end) (\ t537 ->
                                                                                                                                                                                                                                                                t536))) t545
                                                                                                                                                                                                                                       else
                                                                                                                                                                                                                                         end) (xclass []),
                                                                                                                                                                                                                              paraG (\ xL t642 ->
                                                                                                                                                                                                                                       (\ xfv19 t644 ->
                                                                                                                                                                                                                                          if xL == (LConst "Class") then
                                                                                                                                                                                                                                            paraG (\ xL t633 t635 ->
                                                                                                                                                                                                                                                     if xL == (LConst "is_persistent") then
                                                                                                                                                                                                                                                       (paraG (\ xL t624 t626 ->
                                                                                                                                                                                                                                                                 if xL == (LConst "Boolean") then
                                                                                                                                                                                                                                                                   (paraG (\ xL t615 t617 ->
                                                                                                                                                                                                                                                                             if xL == (LBool True) then
                                                                                                                                                                                                                                                                               paraG (\ xL t606 t608 ->
                                                                                                                                                                                                                                                                                        if xL == (LConst "attrs") then
                                                                                                                                                                                                                                                                                          (paraG (\ xL t597 t599 ->
                                                                                                                                                                                                                                                                                                    if xL == (LConst "Attribute") then
                                                                                                                                                                                                                                                                                                      (paraG (\ xL t588 t590 ->
                                                                                                                                                                                                                                                                                                                if xL == (LConst "name") then
                                                                                                                                                                                                                                                                                                                  (paraG (\ xL t579 t581 ->
                                                                                                                                                                                                                                                                                                                            if xL == (LConst "String") then
                                                                                                                                                                                                                                                                                                                              (paraG (\ xaname t570 t572 ->
                                                                                                                                                                                                                                                                                                                                        paraG (\ xL t563 t565 ->
                                                                                                                                                                                                                                                                                                                                                 if xL == (LConst "name") then
                                                                                                                                                                                                                                                                                                                                                   if xcname == xaname then
                                                                                                                                                                                                                                                                                                                                                     (LConst "fkeys") >: (LConst "Fkey") >: (unionsG [(LConst "cols") >: (xcols []),
                                                                                                                                                                                                                                                                                                                                                                                                      (LConst "ref") >: t563])
                                                                                                                                                                                                                                                                                                                                                   else
                                                                                                                                                                                                                                                                                                                                                     end
                                                                                                                                                                                                                                                                                                                                                 else
                                                                                                                                                                                                                                                                                                                                                   end) (xfv19 []))) t579
                                                                                                                                                                                                                                                                                                                            else
                                                                                                                                                                                                                                                                                                                              end)) t588
                                                                                                                                                                                                                                                                                                                else
                                                                                                                                                                                                                                                                                                                  end)) t597
                                                                                                                                                                                                                                                                                                    else
                                                                                                                                                                                                                                                                                                      end)) t606
                                                                                                                                                                                                                                                                                        else
                                                                                                                                                                                                                                                                                          end) (xfv19 [])
                                                                                                                                                                                                                                                                             else
                                                                                                                                                                                                                                                                               end)) t624
                                                                                                                                                                                                                                                                 else
                                                                                                                                                                                                                                                                   end)) t633
                                                                                                                                                                                                                                                     else
                                                                                                                                                                                                                                                       end) (xfv19 [])
                                                                                                                                                                                                                                          else
                                                                                                                                                                                                                                            end) (\ t643 ->
                                                                                                                                                                                                                                                    t642)) (xdests [])])) t664
                                                                                                                                                                                                         else
                                                                                                                                                                                                           end)) t673
                                                                                                                                                                                             else
                                                                                                                                                                                               end)) t682
                                                                                                                                                                                 else
                                                                                                                                                                                   end) (xcols [])
                                                                                                                                                                      else
                                                                                                                                                                        end) (\ t692 ->
                                                                                                                                                                                t691)) (xtable [])))
                                                                                                                                              else
                                                                                                                                                xL41 >: (xtable [])) (\ t704 ->
                                                                                                                                                                        t703)) (paraG (\ xL t257 t259 ->
                                                                                                                                                                                         if xL == (LConst "name") then
                                                                                                                                                                                           (LConst "Table") >: (unionsG [(LConst "name") >: t257,
                                                                                                                                                                                                                         paraG (\ xL t243 t245 ->
                                                                                                                                                                                                                                  if xL == (LConst "Class") then
                                                                                                                                                                                                                                    (paraG (\ xL t234 t236 ->
                                                                                                                                                                                                                                              if xL == (LConst "attrs") then
                                                                                                                                                                                                                                                (paraG (\ xL t225 ->
                                                                                                                                                                                                                                                          (\ xfv46 t227 ->
                                                                                                                                                                                                                                                             if xL == (LConst "Attribute") then
                                                                                                                                                                                                                                                               paraG (\ xL t216 t218 ->
                                                                                                                                                                                                                                                                        if xL == (LConst "name") then
                                                                                                                                                                                                                                                                          paraG (\ xL t207 t209 ->
                                                                                                                                                                                                                                                                                   if xL == (LConst "type") then
                                                                                                                                                                                                                                                                                     (LConst "cols") >: (LConst "Column") >: (unionsG [(LConst "name") >: t216,
                                                                                                                                                                                                                                                                                                                                       (LConst "type") >: t207])
                                                                                                                                                                                                                                                                                   else
                                                                                                                                                                                                                                                                                     end) (xfv46 [])
                                                                                                                                                                                                                                                                        else
                                                                                                                                                                                                                                                                          end) (xfv46 [])
                                                                                                                                                                                                                                                             else
                                                                                                                                                                                                                                                               end) (\ t226 ->
                                                                                                                                                                                                                                                                       t225))) t234
                                                                                                                                                                                                                                              else
                                                                                                                                                                                                                                                end)) t243
                                                                                                                                                                                                                                  else
                                                                                                                                                                                                                                    end) (unionsG [(LConst "Class") >: (xclass []),
                                                                                                                                                                                                                                                   xdests []])])
                                                                                                                                                                                         else
                                                                                                                                                                                           end) (xclass t706))) (\ t185 ->
                                                                                                                                                                                                                   (nthM 0) (gparaG 9 (\ xL146 t181 ->
                                                                                                                                                                                                                                         (\ xdest t183 ->
                                                                                                                                                                                                                                            if xL146 == (LConst "Association") then
                                                                                                                                                                                                                                              return
                                                                                                                                                                                                                                                [end,
                                                                                                                                                                                                                                                 end,
                                                                                                                                                                                                                                                 end,
                                                                                                                                                                                                                                                 end,
                                                                                                                                                                                                                                                 end,
                                                                                                                                                                                                                                                 (nthM 4) t183,
                                                                                                                                                                                                                                                 end,
                                                                                                                                                                                                                                                 end,
                                                                                                                                                                                                                                                 (nthM 7) t183]
                                                                                                                                                                                                                                            else
                                                                                                                                                                                                                                              if xL146 == (LConst "Class") then
                                                                                                                                                                                                                                                return
                                                                                                                                                                                                                                                  [end,
                                                                                                                                                                                                                                                   end,
                                                                                                                                                                                                                                                   end,
                                                                                                                                                                                                                                                   unionsG [(nthM 1) t183,
                                                                                                                                                                                                                                                            (LConst "Class") >: (xdest [])],
                                                                                                                                                                                                                                                   end,
                                                                                                                                                                                                                                                   end,
                                                                                                                                                                                                                                                   unionsG [(nthM 2) t183,
                                                                                                                                                                                                                                                            (LConst "Class") >: (xdest [])],
                                                                                                                                                                                                                                                   end,
                                                                                                                                                                                                                                                   end]
                                                                                                                                                                                                                                              else
                                                                                                                                                                                                                                                if xL146 == (LConst "dest") then
                                                                                                                                                                                                                                                  return
                                                                                                                                                                                                                                                    [end,
                                                                                                                                                                                                                                                     end,
                                                                                                                                                                                                                                                     end,
                                                                                                                                                                                                                                                     end,
                                                                                                                                                                                                                                                     (nthM 3) t183,
                                                                                                                                                                                                                                                     end,
                                                                                                                                                                                                                                                     end,
                                                                                                                                                                                                                                                     (nthM 6) t183,
                                                                                                                                                                                                                                                     end]
                                                                                                                                                                                                                                                else
                                                                                                                                                                                                                                                  if xL146 == (LConst "src_of") then
                                                                                                                                                                                                                                                    return
                                                                                                                                                                                                                                                      [(nthM 5) t183,
                                                                                                                                                                                                                                                       (nthM 8) t183,
                                                                                                                                                                                                                                                       (nthM 8) t183,
                                                                                                                                                                                                                                                       end,
                                                                                                                                                                                                                                                       end,
                                                                                                                                                                                                                                                       end,
                                                                                                                                                                                                                                                       end,
                                                                                                                                                                                                                                                       end,
                                                                                                                                                                                                                                                       end]
                                                                                                                                                                                                                                                  else
                                                                                                                                                                                                                                                    return
                                                                                                                                                                                                                                                      [end,
                                                                                                                                                                                                                                                       end,
                                                                                                                                                                                                                                                       end,
                                                                                                                                                                                                                                                       end,
                                                                                                                                                                                                                                                       end,
                                                                                                                                                                                                                                                       end,
                                                                                                                                                                                                                                                       end,
                                                                                                                                                                                                                                                       end,
                                                                                                                                                                                                                                                       end]) (\ t182 ->
                                                                                                                                                                                                                                                                t181)) (xclass t185)))) []
                                                                                                                            else
                                                                                                                              end)) t721
                                                                                                                else
                                                                                                                  end)) t730
                                                                                                    else
                                                                                                      end) (xclass [])
                                                                                         else
                                                                                           end) (\ t795 ->
                                                                                                   t794)) ((nthM 0) (gparaG 3 (\ xL70 t786 ->
                                                                                                                                 (\ xasc t788 ->
                                                                                                                                    if xL70 == (LConst "Association") then
                                                                                                                                      return
                                                                                                                                        [(nthM 2) t788,
                                                                                                                                         end,
                                                                                                                                         end]
                                                                                                                                    else
                                                                                                                                      if xL70 == (LConst "dest") then
                                                                                                                                        return
                                                                                                                                          [end,
                                                                                                                                           end,
                                                                                                                                           unionsG [(nthM 1) t788,
                                                                                                                                                    xasc []]]
                                                                                                                                      else
                                                                                                                                        if xL70 == (LConst "src") then
                                                                                                                                          return
                                                                                                                                            [end,
                                                                                                                                             end,
                                                                                                                                             unionsG [(nthM 1) t788,
                                                                                                                                                      xasc []]]
                                                                                                                                        else
                                                                                                                                          return
                                                                                                                                            [end,
                                                                                                                                             end,
                                                                                                                                             end]) (\ t787 ->
                                                                                                                                                      t786)) (xdb t797))))
