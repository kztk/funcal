{-# LANGUAGE RecursiveDo #-}

-- the same example as "fscd.hs", but this used
-- the experimental syntax for "RecursiveDo". 

import Language.FUnCAL 
import Control.Monad
import Data.Monoid
import Control.Monad.Fix 

ctail :: (MonadFix m, Memoizing m) => G m a -> m (G m a)
ctail t = do 
  rec f <- srec $ \a x -> return x
  f t 

ctail' :: (MonadFix m, Memoizing m) => G m a -> m (G m a)
ctail' t = do
  rec f <- srec $ \a x -> g x
      g <- srec $ \a x -> a <:> return x
  f t 

input1 :: (MonadFix m, Memoizing m) => m (G m Int) 
input1 = do
  rec a <- graph $ 1 <:> 2 <:> return a
  return a

isHeadA :: (MonadFix m, Memoizing m) => G m String -> m Bool
isHeadA t = do
  rec f <- srec $ \a x ->
        if a == "A" then "dummy" <:> end
        else             end
  not <$> (f t >>= isEmptyG)

hasAA :: (MonadFix m, Memoizing m) => G m String -> m Bool 
hasAA t = do
  rec f <- srec $ \a x ->
        if a == "A" then g x
        else             f x
      g <- srec $ \a x ->
        if a == "A" then "dummy" <:> end
        else             end 
  not <$> (f t >>= isEmptyG)

input2 :: (MonadFix m, Memoizing m) => m (G m String)
input2 = do
  rec a <- graph $ "A" <:> return a
  return a

input3 :: (MonadFix m, Memoizing m) => m (G m String)
input3 = 
  "B" <:> "A" <:> "A" <:> "B" <:> end

res1  = testF ctail  input1
res1' = testF ctail' input1

res21 = testF isHeadA input2
res22 = testF isHeadA input3
res23 = testF hasAA   input2
res24 = testF hasAA   input3

input4 :: (MonadFix m, Memoizing m) => m [G m String]
input4 = do
  rec a <- graph $ mconcat [ "name"  <:> "alice" <:> end
                           , "knows" <:> return c ]
      b <- graph $ mconcat [ "name"  <:> "bob" <:> end
                           , "knows" <:> return a ]
      c <- graph $ mconcat [ "name"  <:> "carol" <:> end
                           , "knows" <:> (return a <> return b) ]
  return [a, b, c]


collect t = do
  rec f <- srec $ \a x -> if a == "name" then "nm" <:> return x
                          else if a == "knows" then f x
                               else                 end
  f t

res3 = testF collect ((!! 0) <$> input4)  
  

testF f t = do
  x <- t
  r <- f x
  putStrLn "Input:"
  print x
  putStrLn "\nOutput:" 
  print r
  
