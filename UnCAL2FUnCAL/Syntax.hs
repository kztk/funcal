module Syntax where 

data Info b a = Info a b deriving Show 
class ExtraInfo f where 
    info :: f a -> a 

instance ExtraInfo (Info b) where 
    info (Info x _) = x 

body (Info _ y) = y 

type IExp a = Info (Exp a) a 

data Name = Name String 
          | MName String
          | FName String 
          | IName Int
          | IName' Int 
            deriving (Eq,Ord) 

instance Show Name where 
    show (Name s)   = "x" ++ dropWhile (=='$') s 
    show (MName s)  = "m" ++ dropWhile (=='&') s
    show (FName s)  = s 
    show (IName i)  = "t" ++ show i 
    show (IName' i) = "v" ++ show i 


data Exp a = EEmp 
           | ECons (IExp a) (IExp a) 
           | EUni  (IExp a) (IExp a)
           | ENull 
           | EDUni  (IExp a) (IExp a)
           | EIMark Name (IExp a)
           | EOMark Name 
           | EAt    (IExp a) (IExp a)
           | ECycle (IExp a)
           | EDoc   String
           | EIf    (IExp a) (IExp a)  (IExp a)
           | ERec   (Name,Name,IExp a) (IExp a)
           | ELet   Name (IExp a) (IExp a)
           | EEq    (IExp a) (IExp a) 
           | EVar   Name 
           | ELabel Label
           | EBool  Bool 
           | EIsEmpty (IExp a) 
           | EAnd   (IExp a) (IExp a)
           | EOr    (IExp a) (IExp a)
           | ENot   (IExp a) 
           deriving Show 
             
data Label = LConst String | LString String | LInt Int | LBool Bool 
           deriving Show 

type IFExp a = Info (FExp a) a 

data FExp a = FEmp 
            | FCons (IFExp a) (IFExp a) 
            | FUni  (IFExp a) (IFExp a)
            | FTup  [IFExp a]
            | FPrj  Int Int 
            | FApp  (IFExp a) (IFExp a)
            | FAbs  Name (IFExp a) 
            | FAbsT [Name] (IFExp a)
            | FVar  Name 
            | FFix  Int (IFExp a)
            | FPara Int (IFExp a)
            | FDoc   String
            | FIf    (IFExp a) (IFExp a)  (IFExp a)
            | FEq    (IFExp a) (IFExp a) 
            | FLabel Label
            | FBool  Bool 
            | FIsEmpty (IFExp a) 
            | FAnd   (IFExp a) (IFExp a)
            | FOr    (IFExp a) (IFExp a)
            | FNot   (IFExp a) 
              deriving Show 


data TargetExp
  = TEEnd
  | TECons
  | TEFix  Int 
  | TEPara Int 
  | TEDoc   String
  | TEFun    (TargetExp -> TargetExp)
  | TEFun' Name (TargetExp -> TargetExp) 
  | TELabel Label
  | TEAbs   Name   TargetExp
  | TEAbsT  [Name] TargetExp
  | TEApp     TargetExp TargetExp  -- e1 e2 
  | TEReturn  TargetExp            -- return e1 
  | TEBind    TargetExp TargetExp  -- e1 >>= e2
  | TEIf      TargetExp TargetExp TargetExp
  | TEVar     Name
  | TEMConcat  [TargetExp] -- mconcat  [e1,...,en]
  | TESequence [TargetExp] -- sequence [e1,...,en]
  | TEProj    Int Int
  | TEBin     BinOp TargetExp TargetExp
  | TEBool    Bool 

data BinOp = OpAnd | OpOr | OpEq 

instance Show BinOp where
  show OpAnd = "&&"
  show OpOr  = "||"
  show OpEq  = "=="


-- | Location in a source code 
data SourceRange
    = SourceRange 
      (Int,Int,Int) -- Start 
      (Int,Int,Int) -- End 
      deriving (Eq,Ord)

instance Show SourceRange where
  show (SourceRange (off1,l1,c1) (off2,l2,c2)) 
        | l1 == l2 =
            "Line: " ++ show l1 ++ " (" ++ show c1 ++ "--" ++ show c2 ++ ")"
        | l1 /= l2 =
            "Lines: " ++ show l1 ++ "(" ++ show c1 ++ ") -- " ++ show l2 ++ "(" ++ show c2 ++ ")"
                          

mergeRange (SourceRange s1 _) (SourceRange _ e2) = SourceRange s1 e2  
