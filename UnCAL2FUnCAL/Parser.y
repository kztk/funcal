{
{-# LANGUAGE NoMonomorphismRestriction #-}
module Parser where 

import Lexer
import Syntax 
}

%name pExp  Exp 

%tokentype { Token SourceRange  }
%error     { parseError }

%token 
    ","      { TkSym _ ","  }
    ":"      { TkSym _ ":"  }
    "{"      { TkSym _ "{"  }
    "}"      { TkSym _ "}"  }

    "("      { TkSym _ "("  }
    ")"      { TkSym _ ")"  }

    "U"      { TkSym _ "U" }
    "++"     { TkSym _ "++" }

    ":="     { TkSym _ ":=" }
    "@"      { TkSym _ "@" }
    "cycle"  { TkKey _ "cycle" }
    "rec"    { TkKey _ "rec" }
    "\\"     { TkSym _ "\\" }
    "."      { TkSym _ "." }

    "if"     { TkKey _ "if" }
    "then"   { TkKey _ "then" }
    "else"   { TkKey _ "else" }
    "llet"   { TkKey _ "llet" }
    "let"    { TkKey _ "let" }
    "in"     { TkKey _ "in" }

    "and"    { TkKey _ "and" }
    "or"     { TkKey _ "or" }
    "not"    { TkKey _ "not" }
    
    "isempty" { TkKey _ "isempty" }
    "="      { TkSym _ "="  }
    "<"      { TkSym _ "<"  }
    ">"      { TkSym _ ">"  }

    "doc"    { TkKey _ "doc" }

    bool     { TkBool _ _ }

    ident    { TkIdent  _ _ } 
    marker   { TkMarker _ _ } 
    string   { TkString _ _ }
    letters  { TkLetters _ _ }
    number   { TkNumber _ _ }

%left     ","
%right    "++"
%left     ":="
%nonassoc "in"
%right    "U" 
%nonassoc "then" "else"
%left     "@"
%left     "or"
%left     "and"
%nonassoc "not"
%left     "="
%nonassoc "<" ">"


%%

Exp :: { IExp SourceRange }
Exp : "{" "}"         { Info (m $1 $2) $ EEmp  }
    | "{" Edges "}"   {  foldr1 (\x y -> Info (m x y) $ EUni x y) $2 }
    | Exp "U" Exp     { Info (m $1 $3) $ EUni   $1 $3 }
    | Exp "++" Exp    { Info (m $1 $3) $ EDUni  $1 $3 }
    | marker ":=" Exp { Info (m $1 $3) $ EIMark (MName $ str $1) $3 }
    | marker                  { Info (info $1) $ EOMark (MName $ str $1) }
    | "(" ")"                 { Info (m $1 $2) $ ENull }
    | "(" Exps ")"    { foldr1 (\x y -> Info (m x y) $ EDUni x y) $2 }
    | Exp "@" Exp             { Info (m $1 $3) $ EAt $1 $3 }
    | "cycle" "(" Exp ")"     { Info (m $1 $4) $ ECycle $3 }
    | "doc" "(" string ")"    { Info (m $1 $4) $ EDoc (str $3) }
    | "if" BExp "then" Exp "else" Exp 
      { Info (m $1 $6) $ EIf $2 $4 $6 } 
    | "rec" "(" RecBody ")" Exp { Info (m $1 $5) $ ERec $3 $5 }
    | "let" ident "=" Exp "in" Exp  { Info (m $1 $6) $ ELet (Name $ str $2) $4 $6 }
    | "llet" ident "=" Exp "in" Exp { Info (m $1 $6) $ ELet (Name $ str $2) $4 $6 }
    | "isempty" "(" Exp ")"         { Info (m $1 $4) $ EIsEmpty $3 }
    | ident                         { Info (info $1) $ EVar  (Name $ str $1) }
    | Label                         { let i = Info (info $1) in i $ ECons $1 (i EEmp) }
--     | "{" Label "}"                 { let i = Info (info $2) in i $ ECons $2 (i EEmp) }


LExp : Label { $1 } 
     | ident { Info (info $1) $ EVar  (Name $ str $1) }

BExp : LExp "=" LExp                  { Info (m $1 $3) $ EEq $1 $3 } 
     | "isempty" "(" Exp ")"          { Info (m $1 $4) $ EIsEmpty $3 }
     | bool                           { Info (info $1) $ EBool (bool $1) }
     | BExp "and" BExp                { Info (m $1 $3) $ EAnd $1 $3 }
     | "not" BExp                     { Info (info $2) $ ENot $2    }
     | BExp "or"  BExp                { Info (m $1 $3) $ EOr  $1 $3 }

RecBody : "\\" "(" ident "," ident ")" "." Exp { (Name $ str $3, Name $ str $5,$8) }
            
Edges : Edge           { [$1] }
      | Edge "," Edges {$1:$3}

Edge : LExp ":" Exp { Info (m $1 $3) $ ECons $1 $3 }
     | Label        { let i = Info (info $1) in i $ ECons $1 (i EEmp) }

Exps : Exp          { [$1] }
     | Exp "," Exps { $1:$3 }

Label : string                        { Info (info $1) $ ELabel (LString $ str $1) }
      | number                        { Info (info $1) $ ELabel (LInt    $ num $1) }
      | letters                       { Info (info $1) $ ELabel (LConst $ str $1) }
      | bool                          { Info (info $1) $ ELabel (LBool $ bool $1) }


{
num (TkNumber _ n) = n 
str (TkString _ n) = n 
str (TkIdent _ n) = n 
str (TkLetters _ n) = n 
str (TkMarker _ n) = n 

bool (TkBool _ n) = n

m x y = mergeRange (info x) (info y)

parseError :: [Token SourceRange] -> a
parseError [] = error $ "Parse error at the end" 
parseError (tok@(t:_)) = error $ 
    let r = info t 
    in "Parse Error at : " ++ show r ++ "\n" ++ 
       "            near tokens: " ++ show (take 5 tok)
                                   ++ if length tok > 5 then " ... " else "" 

parseExp  = pExp  . alexScanTokens
}

